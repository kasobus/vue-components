import Vue from 'vue'
import TabbedViewer from './components/tabbed-viewer.vue'
import PhysicianFinder from './components/physician-finder.vue'
import PhysicianListing from './components/physician-listing.vue'
Vue.config.devtools = true
var vues = document.getElementsByClassName("vue-component");
for(var i = 0; i < vues.length; i++)
{
  vues[i].id = "vue-"+i;
  new Vue({
    el: "#"+vues[i].id,	
    components: {
      'tabbed-viewer': TabbedViewer,
      'physician-finder': PhysicianFinder,
      'physician-listing': PhysicianListing
    }
  });
}
